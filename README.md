KP Toolkit
==========

Overview
--------
A collection of utilities useful for Mac and iOS development.

KPTASL
------
Wrappers for Apple System Log (ASL) search related APIs and data structures.
For writing to ASL, use GTMLogger from the excellent `Google Toolbox for Mac`_.

.. _Google Toolbox for Mac: http://code.google.com/p/google-toolbox-for-mac/
