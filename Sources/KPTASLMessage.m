//
//  KPTASLMessage.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "KPTASLMessage.h"

@interface KPTASLMessage ()

- (id)initWithResponseMessage:(aslmsg)message;

@end

@implementation KPTASLMessage

@synthesize message = message_;

+ (id)message
{
  return [[[self alloc] init] autorelease];
}

+ (id)messageWithResponseMessage:(aslmsg)message
{
  return [[[self alloc] initWithResponseMessage:message] autorelease];
}

- (id)init
{
  if ((self = [super init]))
  {
    message_ = asl_new(ASL_TYPE_MSG);
    if (message_ == nil)
    {
      [self release];
      self = nil;
    }
  }

  return self;
}

- (id)initWithResponseMessage:(aslmsg)message
{
  if ((self = [super init]))
  {
    message_ = message;
    nofree_ = YES;
    if (message_ == nil)
    {
      [self release];
      self = nil;
    }
  }

  return self;
}

- (void)dealloc
{
  if (message_ != NULL && nofree_ != YES)
  {
    asl_free(message_);
  }

  [super dealloc];
}

- (NSString *)valueForProperty:(NSString *)property
{
  if (property == nil)
  {
    @throw [NSException exceptionWithName:NSInvalidArgumentException
                                   reason:@"Invalid parameter not satisfying "
                                           "'property != nil'"
                                 userInfo:nil];
  }

  const char *value = asl_get([self message], [property UTF8String]);
  if (value == NULL)
  {
    return nil;
  }

  return [NSString stringWithUTF8String:value];
}

- (BOOL)setValue:(NSString *)value
     forProperty:(NSString *)property
{
  if (property == nil)
  {
    @throw [NSException exceptionWithName:NSInvalidArgumentException
                                   reason:@"Invalid parameter not satisfying "
                                           "'property != nil'"
                                 userInfo:nil];
  }

  int result = 0;
  if (value == nil)
  {
    result = asl_unset([self message], [property UTF8String]);
  }
  else
  {
    result = asl_set([self message], [property UTF8String], [value UTF8String]);
  }
  return result == 0;
}

- (NSDictionary *)properties
{
  NSMutableDictionary *properties = [NSMutableDictionary dictionary];
  aslmsg message = [self message];
  const char *key = nil, *value = nil;
  uint32_t i = 0;
  for (i = 0; (key = asl_key(message, i)) != NULL; ++i)
  {
    value = asl_get(message, key);
    NSString *stringValue = value ? [NSString stringWithUTF8String:value] :
                                    [NSString string];
    [properties setObject:stringValue
                   forKey:[NSString stringWithUTF8String:key]];
  }
  return properties;
}

@end
