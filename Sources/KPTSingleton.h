//
//  KPTSingleton.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

// Macros for defining a singleton (based on the singleton implementation
// posted by Matt Gallagher on CocoaWithLove).
//
// Define the singleton in the interface file as -
// KPT_DECLARE_SINGLETON_FOR_CLASS(MyClass)
//
// In the implementation file -
// KPT_SYNTHESIZE_SINGLETON_FOR_CLASS(MyClass)

#import <objc/runtime.h>

#import "KPTDefines.h"

#define KPT_DECLARE_SINGLETON_FOR_CLASS_WITH_ACCESSOR(cls, accessorMethod)  \
  + (cls *)accessorMethod;


#if __has_feature(objc_arc)
  #define KPT_SYNTHESIZE_SINGLETON_RETAIN_METHODS
#else
  #define KPT_SYNTHESIZE_SINGLETON_RETAIN_METHODS     \
  - (id)retain                                        \
  {                                                   \
    return self;                                      \
  }                                                   \
                                                      \
  - (NSUInteger)retainCount                           \
  {                                                   \
    return NSUIntegerMax;                             \
  }                                                   \
                                                      \
  - (oneway void)release                              \
  {                                                   \
  }                                                   \
                                                      \
  - (id)autorelease                                   \
  {                                                   \
    return self;                                      \
  }
#endif // objc_arc


#define KPT_SYNTHESIZE_SINGLETON_FOR_CLASS_WITH_ACCESSOR(cls, accessorMethod) \
                                                                              \
static cls *accessorMethod##Instance = nil;                                   \
                                                                              \
+ (cls *)accessorMethod                                                       \
{                                                                             \
  @synchronized(self)                                                         \
  {                                                                           \
    if (accessorMethod##Instance == nil)                                      \
    {                                                                         \
      accessorMethod##Instance = [super allocWithZone:NULL];                  \
      accessorMethod##Instance = [accessorMethod##Instance init];             \
      method_exchangeImplementations(                                         \
        class_getClassMethod(                                                 \
          [accessorMethod##Instance class],                                   \
          @selector(accessorMethod)                                           \
        ),                                                                    \
        class_getClassMethod(                                                 \
          [accessorMethod##Instance class],                                   \
          @selector(kpt_lockless_##accessorMethod)                            \
        )                                                                     \
      );                                                                      \
      method_exchangeImplementations(                                         \
        class_getInstanceMethod(                                              \
          [accessorMethod##Instance class],                                   \
          @selector(init)                                                     \
        ),                                                                    \
        class_getInstanceMethod(                                              \
          [accessorMethod##Instance class],                                   \
          @selector(kpt_onlyInitOnce)                                         \
        )                                                                     \
      );                                                                      \
    }                                                                         \
  }                                                                           \
                                                                              \
  return accessorMethod##Instance;                                            \
}                                                                             \
                                                                              \
+ (cls *)kpt_lockless_##accessorMethod                                        \
{                                                                             \
  return accessorMethod##Instance;                                            \
}                                                                             \
                                                                              \
+ (id)allocWithZone:(NSZone *) KPT_UNUSED zone                                \
{                                                                             \
  return [[self accessorMethod] retain];                                      \
}                                                                             \
                                                                              \
- (id)copyWithZone:(NSZone *) KPT_UNUSED zone                                 \
{                                                                             \
  return self;                                                                \
}                                                                             \
                                                                              \
- (id)kpt_onlyInitOnce                                                        \
{                                                                             \
  return self;                                                                \
}                                                                             \
                                                                              \
KPT_SYNTHESIZE_SINGLETON_RETAIN_METHODS


#define KPT_DECLARE_SINGLETON_FOR_CLASS(cls)      \
  KPT_DECLARE_SINGLETON_FOR_CLASS_WITH_ACCESSOR(cls, shared##cls)


#define KPT_SYNTHESIZE_SINGLETON_FOR_CLASS(cls)   \
  KPT_SYNTHESIZE_SINGLETON_FOR_CLASS_WITH_ACCESSOR(cls, shared##cls)
