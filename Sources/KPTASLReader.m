//
//  KPTASLReader.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "KPTASLReader.h"

#import "KPTASLMessage.h"
#import "KPTASLQuery.h"
#import "KPTASLResponse.h"

@interface KPTASLReader ()

- (id)initWithQuery:(KPTASLQuery *)query
             writer:(id<KPTASLReaderWriter>)writer
          formatter:(id<KPTASLReaderFormatter>)formatter;

@property (nonatomic, readonly) KPTASLQuery *query;
@property (nonatomic, readonly) id<KPTASLReaderWriter> writer;
@property (nonatomic, readonly) id<KPTASLReaderFormatter> formatter;

@end

@implementation KPTASLReader

@synthesize query = query_;
@synthesize writer = writer_;
@synthesize formatter = formatter_;

+ (id)readerWithQuery:(KPTASLQuery *)query
               writer:(id<KPTASLReaderWriter>)writer
            formatter:(id<KPTASLReaderFormatter>)formatter
{
  return [[[self alloc] initWithQuery:query
                               writer:writer
                            formatter:formatter] autorelease];
}

- (id)initWithQuery:(KPTASLQuery *)query
             writer:(id<KPTASLReaderWriter>)writer
          formatter:(id<KPTASLReaderFormatter>)formatter
{
  if ((self = [super init]))
  {
    query_ = [query retain];
    if (query_ == nil)
    {
      [self release];
      return nil;
    }
    writer_ = [writer retain];
    if (writer_ == nil)
    {
      writer_ = [[NSFileHandle fileHandleWithStandardOutput] retain];
    }
    formatter_ = [formatter retain];
    if (formatter_ == nil)
    {
      formatter_ = [[KPTASLReaderBasicFormatter basicFormatter] retain];
    }
  }

  return self;
}

- (void)dealloc
{
  [query_ release];
  query_ = nil;
  [writer_ release];
  writer_ = nil;
  [formatter_ release];
  formatter_ = nil;

  [super dealloc];
}

- (void)main
{
  KPTASLResponse *response = [KPTASLResponse responseFromQuery:[self query]];
  for (KPTASLMessage *message = [response next];
       [self isCancelled] != YES && message != nil;
       message = [response next])
  {
    [[self writer] write:[[self formatter] stringForMessage:message]];
  }
}

@end

#pragma mark -
#pragma mark KPTASLReaderWriter

@implementation NSFileHandle (KPTASLReaderWriter)

+ (id)fileHandleForWritingAtPath:(NSString *)path
                            mode:(mode_t)mode
{
  int fd = -1;
  if (path != nil)
  {
    int flags = O_WRONLY | O_APPEND | O_CREAT;
    fd = open([path fileSystemRepresentation], flags, mode);
  }
  if (fd == -1)
  {
    return nil;
  }
  return [[[self alloc] initWithFileDescriptor:fd
                                closeOnDealloc:YES] autorelease];
}

- (void)write:(NSString *)message
{
  @synchronized(self)
  {
    NSString *line = [message stringByAppendingString:@"\n"];
    [self writeData:[line dataUsingEncoding:NSUTF8StringEncoding]];
  }
}

@end

#pragma mark -
#pragma mark KPTASLReaderBasicFormatter

@implementation KPTASLReaderBasicFormatter

+ (id)basicFormatter
{
  return [[[self alloc] init] autorelease];
}

- (id)init
{
  if ((self = [super init]))
  {
    numberFormatter_ = [[NSNumberFormatter alloc] init];
    dateFormatter_ = [[NSDateFormatter alloc] init];
    [dateFormatter_ setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter_ setTimeStyle:NSDateFormatterLongStyle];
  }
  return self;
}

- (void)dealloc
{
  [numberFormatter_ release];
  numberFormatter_ = nil;
  [dateFormatter_ release];
  dateFormatter_ = nil;

  [super dealloc];
}

- (NSString *)stringForMessage:(KPTASLMessage *)message
{
  NSDictionary *properties = [message properties];
  NSMutableArray *formattedValues =
    [NSMutableArray arrayWithCapacity:[properties count]];
  __block NSString *formattedMessage = nil;

  [properties enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
    (void)stop;
    NSString *formattedValue = [self stringForMessageProperty:key
                                                        value:obj];
    if ([formattedValue length] > 0)
    {
      // Time should be first.
      if ([key isEqualToString:kKPTASLMessagePropertyTime])
      {
        [formattedValues insertObject:formattedValue
                              atIndex:0];
      }
      // Message will be inserted at the end.
      else if ([key isEqualToString:kKPTASLMessagePropertyMessage])
      {
        formattedMessage = formattedValue;
      }
      else
      {
        [formattedValues addObject:formattedValue];
      }
    }
  }];

  if (formattedMessage)
  {
    [formattedValues addObject:formattedMessage];
  }

  return [formattedValues componentsJoinedByString:@" "];
}

- (NSString *)stringForMessageProperty:(NSString *)property
                                 value:(NSString *)value
{
  NSString *string = value ? value : @"";
  if ([property isEqualToString:kKPTASLMessagePropertyTime])
  {
    NSNumber *number = [numberFormatter_ numberFromString:value];
    if (number != nil)
    {
      NSTimeInterval timeInterval = [number doubleValue];
      NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
      string = [dateFormatter_ stringFromDate:date];
    }
  }

  return string;
}

@end

#pragma mark KPTASLReaderPropertyFormatter

@interface KPTASLReaderPropertyFormatter ()

- (id)initWithProperties:(NSArray *)properties;

@end

@implementation KPTASLReaderPropertyFormatter

+ (id)propertyFormatterWithProperties:(NSArray *)properties
{
  // Xcode4 is giving a build error without the explicit typecast.
  return [[(KPTASLReaderPropertyFormatter *)[self alloc]
           initWithProperties:properties] autorelease];
}

- (id)initWithProperties:(NSArray *)properties
{
  if ((self = [super init]))
  {
    properties_ = [properties retain];
  }

  return self;
}

- (void)dealloc
{
  [properties_ release];
  properties_ = nil;

  [super dealloc];
}

- (NSString *)stringForMessage:(KPTASLMessage *)message
{
  NSMutableArray *formattedValues =
    [NSMutableArray arrayWithCapacity:[properties_ count]];
  for (NSString *property in properties_)
  {
    NSString *value = [message valueForProperty:property];
    NSString *formattedValue = [self stringForMessageProperty:property
                                                        value:value];
    if ([formattedValue length] > 0)
    {
      [formattedValues addObject:formattedValue];
    }
  }
  return [formattedValues componentsJoinedByString:@" "];
}

@end
