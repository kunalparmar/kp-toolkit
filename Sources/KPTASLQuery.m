//
//  KPTASLQuery.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "KPTASLQuery.h"

@implementation KPTASLQuery

@synthesize query = query_;

+ (id)query
{
  return [[[self alloc] init] autorelease];
}

- (id)init
{
  if ((self = [super init]))
  {
    query_ = asl_new(ASL_TYPE_QUERY);
    if (query_ == nil)
    {
      [self release];
      self = nil;
    }
  }

  return self;
}

- (void)dealloc
{
  asl_free(query_);

  [super dealloc];
}

- (NSString *)valueForProperty:(NSString *)property
{
  if (property == nil)
  {
    @throw [NSException exceptionWithName:NSInvalidArgumentException
                                   reason:@"Invalid parameter not satisfying "
                                           "'property != nil'"
                                 userInfo:nil];
  }

  const char *value = asl_get([self query], [property UTF8String]);
  if (value == NULL)
  {
    return nil;
  }

  return [NSString stringWithUTF8String:value];
}

- (BOOL)setValue:(NSString *)value
       operation:(KPTASLQueryOperation)operation
     forProperty:(NSString *)property
{
  if (property == nil)
  {
    @throw [NSException exceptionWithName:NSInvalidArgumentException
                                   reason:@"Invalid parameter not satisfying "
                                           "'property != nil'"
                                 userInfo:nil];
  }

  int result = 0;
  if (value == nil)
  {
    result = asl_unset([self query], [property UTF8String]);
  }
  else
  {
    result = asl_set_query([self query],
                           [property UTF8String],
                           [value UTF8String],
                           operation);
  }
  return result == 0;
}

@end
