//
//  KPTASLMessageTest.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <SenTestingKit/SenTestingKit.h>

#import "KPTASLMessage.h"

@interface KPTASLMessageTest : SenTestCase
{
  KPTASLMessage *message;
}

@end

@implementation KPTASLMessageTest

- (void)setUp
{
  message = [[KPTASLMessage message] retain];
  STAssertNotNil(message, nil);
}

- (void)tearDown
{
  STAssertNotNil(message, nil);
  [message release];
  message = nil;
}

- (void)testCreation
{
  STAssertNil([KPTASLMessage messageWithResponseMessage:NULL], nil);
}

- (void)testValueForProperty
{
  STAssertThrowsSpecificNamed([message valueForProperty:nil],
                              NSException,
                              NSInvalidArgumentException,
                              nil);
  STAssertNil([message valueForProperty:@"foo"], nil);
}

- (void)testSetValueForProperty
{
  STAssertThrowsSpecificNamed([message setValue:@"foo"
                                    forProperty:nil],
                              NSException,
                              NSInvalidArgumentException,
                              nil);

  STAssertTrue([message setValue:@"foo"
                     forProperty:@"bar"],
               nil);
  STAssertTrue([message setValue:@"blah"
                     forProperty:@"baz"],
               nil);
  STAssertEqualObjects([message valueForProperty:@"bar"], @"foo", nil);
  STAssertEqualObjects([message valueForProperty:@"baz"], @"blah", nil);

  NSDictionary *properties = [message properties];
  STAssertEqualObjects([properties objectForKey:@"bar"], @"foo", nil);
  STAssertEqualObjects([properties objectForKey:@"baz"], @"blah", nil);

  STAssertTrue([message setValue:nil
                     forProperty:@"bar"],
               nil);
  STAssertNil([message valueForProperty:@"bar"], nil);
}

@end
