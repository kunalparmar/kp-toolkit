//
//  KPTASLResponse.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "KPTASLResponse.h"

#import "KPTASLMessage.h"
#import "KPTASLQuery.h"

@interface KPTASLResponse ()

- (id)initWithResponse:(aslresponse)response;

@end

@implementation KPTASLResponse

@synthesize response = response_;

+ (id)responseFromQuery:(KPTASLQuery *)query
{
  if (query == nil)
  {
    return nil;
  }

  aslresponse response = asl_search(NULL, [query query]);
  return [[[self alloc] initWithResponse:response] autorelease];
}

- (id)initWithResponse:(aslresponse)response
{
  if ((self = [super init]))
  {
    response_ = response;
    if (response_ == nil)
    {
      [self release];
      self = nil;
    }
  }

  return self;
}

- (void)dealloc
{
  if (response_ != NULL)
  {
    aslresponse_free(response_);
  }

  [super dealloc];
}

- (KPTASLMessage *)next
{
  aslmsg message = aslresponse_next([self response]);
  return [KPTASLMessage messageWithResponseMessage:message];
}

@end
