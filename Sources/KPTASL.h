//
//  KPTASL.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#ifndef KPTASL_H
#define KPTASL_H

#import <asl.h>

//
// Wrappers for constants from asl.h.
//

//
// Attributes associated with each aslmsg.
//
extern NSString * const kKPTASLMessagePropertyTime;
extern NSString * const kKPTASLMessagePropertyHost;
extern NSString * const kKPTASLMessagePropertySender;
extern NSString * const kKPTASLMessagePropertyFacility;
extern NSString * const kKPTASLMessagePropertyPID;
extern NSString * const kKPTASLMessagePropertyUID;
extern NSString * const kKPTASLMessagePropertyGID;
extern NSString * const kKPTASLMessagePropertyLevel;
extern NSString * const kKPTASLMessagePropertyMessage;


//
// Logging levels.
//
extern NSString * const kKPTASLLoggingLevelEmergency;
extern NSString * const kKPTASLLoggingLevelAlert;
extern NSString * const kKPTASLLoggingLevelCritical;
extern NSString * const kKPTASLLoggingLevelError;
extern NSString * const kKPTASLLoggingLevelWarning;
extern NSString * const kKPTASLLoggingLevelNotice;
extern NSString * const kKPTASLLoggingLevelInfo;
extern NSString * const kKPTASLLoggingLevelDebug;

//
// Operations used with asl query.
//
typedef enum
{
  //
  // Base operations. They are mutually exclusive.
  //

  // Value equality.
  kKPTASLQueryOperationEqual                = ASL_QUERY_OP_EQUAL,
  // Value greater than.
  kKPTASLQueryOperationGreater              = ASL_QUERY_OP_GREATER,
  // Value greater than or equal to.
  kKPTASLQueryOperationGreaterEqual         = ASL_QUERY_OP_GREATER_EQUAL,
  // Value less than.
  kKPTASLQueryOperationLess                 = ASL_QUERY_OP_LESS,
  // Value less than or equal to.
  kKPTASLQueryOperationLessEqual            = ASL_QUERY_OP_LESS_EQUAL,
  // Value not equal.
  kKPTASLQueryOperationNotEqual             = ASL_QUERY_OP_NOT_EQUAL,
  // Regular expression search using regex library.
  kKPTASLQueryOperationRegularExpression    = ASL_QUERY_OP_REGEX,
  // Always true - used to test for the existence of a key.
  kKPTASLQueryOperationTrue                 = ASL_QUERY_OP_TRUE,

  //
  // Modifiers that change the behvaior of the base operations. Any number of
  // modifiers can be ORed with the base operations.
  //

  // String comparisons are case folded.
  kKPTASLQueryOperationCaseFold             = ASL_QUERY_OP_CASEFOLD,
  // Match a leading substring.
  kKPTASLQueryOperationPrefix               = ASL_QUERY_OP_PREFIX,
  // Match a trailing substring.
  kKPTASLQueryOperationSuffix               = ASL_QUERY_OP_SUFFIX,
  // Match any substring.
  kKPTASLQueryOperationSubstring            = ASL_QUERY_OP_SUBSTRING,
  // Value are converted to an integer using atoi.
  kKPTASLQueryOperationNumeric              = ASL_QUERY_OP_NUMERIC,

}KPTASLQueryOperation;

#endif  // KPTASL_H
