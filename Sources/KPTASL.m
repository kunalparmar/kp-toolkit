//
//  KPTASL.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "KPTASL.h"

NSString * const kKPTASLMessagePropertyTime = @"" ASL_KEY_TIME;
NSString * const kKPTASLMessagePropertyHost = @"" ASL_KEY_HOST;
NSString * const kKPTASLMessagePropertySender = @"" ASL_KEY_SENDER;
NSString * const kKPTASLMessagePropertyFacility = @"" ASL_KEY_FACILITY;
NSString * const kKPTASLMessagePropertyPID = @"" ASL_KEY_PID;
NSString * const kKPTASLMessagePropertyUID = @"" ASL_KEY_UID;
NSString * const kKPTASLMessagePropertyGID = @"" ASL_KEY_GID;
NSString * const kKPTASLMessagePropertyLevel = @"" ASL_KEY_LEVEL;
NSString * const kKPTASLMessagePropertyMessage = @"" ASL_KEY_MSG;


NSString * const kKPTASLLoggingLevelEmergency = @"" ASL_STRING_EMERG;
NSString * const kKPTASLLoggingLevelAlert = @"" ASL_STRING_ALERT;
NSString * const kKPTASLLoggingLevelCritical = @"" ASL_STRING_CRIT;
NSString * const kKPTASLLoggingLevelError = @"" ASL_STRING_ERR;
NSString * const kKPTASLLoggingLevelWarning = @"" ASL_STRING_WARNING;
NSString * const kKPTASLLoggingLevelNotice = @"" ASL_STRING_NOTICE;
NSString * const kKPTASLLoggingLevelInfo = @"" ASL_STRING_INFO;
NSString * const kKPTASLLoggingLevelDebug = @"" ASL_STRING_DEBUG;
