//
//  KPTASLQuery.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <Foundation/Foundation.h>

#import "KPTASL.h"

@interface KPTASLQuery : NSObject
{
  aslmsg query_;
}

@property (nonatomic, readonly) aslmsg query;

// Returns an autoreleased instance.
+ (id)query;

// Returns the value of a given property.
//
// Parameters
//  property
//    The property whose value will be returned. Cannot be nil. Raises an
//    exception if |property| is nil.
- (NSString *)valueForProperty:(NSString *)property;

// Sets the value and operation of a given property.
//
// Parameters
//  value
//    The value to set for given property.
//  operation
//    The operation to use when comparing the value. If value is nil, the
//    property will not be used when the query is run.
//  property
//    The property whose value will be set. Cannot be nil. Raises an
//    exception if |property| is nil.
//
// Return Value
//  YES if the value and operation was set successfully; otherwise, NO.
- (BOOL)setValue:(NSString *)value
       operation:(KPTASLQueryOperation)operation
     forProperty:(NSString *)property;

@end
