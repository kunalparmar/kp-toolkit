//
//  KPTASLReaderTest.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <SenTestingKit/SenTestingKit.h>

#import "KPTASL.h"
#import "KPTASLMessage.h"
#import "KPTASLReader.h"
#import "KPTASLQuery.h"

@interface KPTASLReaderTest : SenTestCase
{
 @private
  NSString *path_;
}

@end

@implementation KPTASLReaderTest

- (void)setUp
{
  path_ = [[NSTemporaryDirectory() stringByAppendingPathComponent:
            NSStringFromClass([self class])] retain];
  STAssertNotNil(path_, nil);

  // Cleanup data from last run.
  [[NSFileManager defaultManager] removeItemAtPath:path_
                                             error:NULL];
}

- (void)tearDown
{
  STAssertNotNil(path_, nil);

  // Cleanup.
  [[NSFileManager defaultManager] removeItemAtPath:path_
                                             error:NULL];

  [path_ release];
  path_ = nil;
}

- (void)testReaderCreation
{
  STAssertNil([KPTASLReader readerWithQuery:nil
                                     writer:nil
                                  formatter:nil],
              nil);
}

- (void)testReader
{
  NSProcessInfo *processInfo = [NSProcessInfo processInfo];
  NSString *loggedString = [NSString stringWithFormat:@"Testing %@: %@",
                            NSStringFromClass([self class]),
                            [processInfo globallyUniqueString]];

  // NSLog output goes to ASL with facility "com.apple.com" and level warning.
  NSLog(@"%@", loggedString);

  KPTASLQuery *query = [KPTASLQuery query];
  STAssertNotNil(query, nil);
  STAssertTrue([query setValue:@"com.apple.console"
                     operation:kKPTASLQueryOperationEqual
                   forProperty:kKPTASLMessagePropertyFacility],
               nil);
  STAssertTrue([query setValue:kKPTASLLoggingLevelWarning
                     operation:kKPTASLQueryOperationEqual
                   forProperty:kKPTASLMessagePropertyLevel],
               nil);
  NSString *pid = [NSString stringWithFormat:@"%d",
                   [processInfo processIdentifier]];
  STAssertNotNil(pid, nil);
  STAssertTrue([query setValue:pid
                     operation:kKPTASLQueryOperationEqual
                   forProperty:kKPTASLMessagePropertyPID],
               nil);
  NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:path_
                                                                 mode:0644];
  STAssertNotNil(fileHandle, nil);
  KPTASLReaderBasicFormatter *formatter =
    [KPTASLReaderBasicFormatter basicFormatter];
  STAssertNotNil(formatter, nil);
  KPTASLReader *reader = [KPTASLReader readerWithQuery:query
                                                writer:fileHandle
                                             formatter:formatter];
  STAssertNotNil(reader, nil);

  NSOperationQueue *readerQueue = [[[NSOperationQueue alloc] init] autorelease];
  STAssertNotNil(readerQueue, nil);
  [readerQueue addOperation:reader];
  [readerQueue waitUntilAllOperationsAreFinished];

  NSError *error = nil;
  NSString *contents = [NSString stringWithContentsOfFile:path_
                                                 encoding:NSUTF8StringEncoding
                                                    error:&error];
  STAssertNotNil(contents, @"Error opening file: %@", error);
  NSRange range = [contents rangeOfString:loggedString];
  STAssertTrue(range.location != NSNotFound, nil);
}

- (void)testFileHandleWriter
{
  NSFileHandle *fileHandle = nil;

  fileHandle = [NSFileHandle fileHandleForWritingAtPath:path_];
  STAssertNil(fileHandle, nil);

  fileHandle = [NSFileHandle fileHandleForWritingAtPath:path_
                                                   mode:0644];
  STAssertNotNil(fileHandle, nil);

  [fileHandle write:@"test 1"];
  [fileHandle write:@"test 2"];
  [fileHandle write:@"test 3"];
  [fileHandle write:@"test 4"];
  [fileHandle closeFile];

  NSError *error = nil;
  NSString *contents = [NSString stringWithContentsOfFile:path_
                                                 encoding:NSUTF8StringEncoding
                                                    error:&error];
  STAssertNotNil(contents, @"Error opening file: %@", error);
  STAssertEqualObjects(@"test 1\ntest 2\ntest 3\ntest 4\n", contents, nil);
}

- (void)testFileHandleCreation
{
  NSFileHandle *fileHandle = nil;

  fileHandle = [NSFileHandle fileHandleForWritingAtPath:path_];
  STAssertNil(fileHandle, nil);

  fileHandle = [NSFileHandle fileHandleForWritingAtPath:nil
                                                   mode:0644];
  STAssertNil(fileHandle, nil);

  fileHandle = [NSFileHandle fileHandleForWritingAtPath:path_
                                                   mode:0644];
  STAssertNotNil(fileHandle, nil);

  [fileHandle write:@"test 1"];
  [fileHandle write:@"test 2"];
  [fileHandle closeFile];

  // Reopen the file and test that the messages get appended.
  fileHandle = [NSFileHandle fileHandleForWritingAtPath:path_
                                                   mode:0644];
  STAssertNotNil(fileHandle, nil);

  [fileHandle write:@"test 3"];
  [fileHandle write:@"test 4"];
  [fileHandle closeFile];

  NSError *error = nil;
  NSString *contents = [NSString stringWithContentsOfFile:path_
                                                 encoding:NSUTF8StringEncoding
                                                    error:&error];
  STAssertNotNil(contents, @"Error opening file: %@", error);
  STAssertEqualObjects(@"test 1\ntest 2\ntest 3\ntest 4\n", contents, nil);
}

- (void)testBasicFormatter
{
  KPTASLReaderBasicFormatter *formatter =
    [[[KPTASLReaderBasicFormatter alloc] init] autorelease];
  STAssertNotNil(formatter, nil);
  STAssertEqualObjects([formatter stringForMessageProperty:nil
                                                     value:nil],
                       @"",
                       nil);
  STAssertEqualObjects([formatter stringForMessageProperty:nil
                                                     value:@"bar"],
                       @"bar",
                       nil);
  STAssertEqualObjects([formatter stringForMessageProperty:@"foo"
                                                     value:nil],
                       @"",
                       nil);
  STAssertEqualObjects([formatter stringForMessageProperty:@"foo"
                                                     value:@"bar"],
                       @"bar",
                       nil);
  STAssertEqualObjects(
                [formatter stringForMessageProperty:kKPTASLMessagePropertyTime
                                              value:nil],
                       @"",
                       nil);
  STAssertEqualObjects(
                [formatter stringForMessageProperty:kKPTASLMessagePropertyTime
                                                     value:@"bar"],
                       @"bar",
                       nil);
  NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
  STAssertNotNil(dateFormatter, nil);
  [dateFormatter setDateStyle:NSDateFormatterShortStyle];
  [dateFormatter setTimeStyle:NSDateFormatterLongStyle];
  NSDate *date = [NSDate date];
  NSString *timeIntervalSince1970 =
    [NSString stringWithFormat:@"%f", [date timeIntervalSince1970]];
  STAssertNotNil(timeIntervalSince1970, nil);
  STAssertEqualObjects(
                [formatter stringForMessageProperty:kKPTASLMessagePropertyTime
                                              value:timeIntervalSince1970],
                       [dateFormatter stringFromDate:date],
                       nil);

  KPTASLMessage *message = [KPTASLMessage message];
  STAssertTrue([message setValue:@"property1Value"
                     forProperty:@"property1"],
               nil);
  STAssertTrue([message setValue:@"property2Value"
                     forProperty:@"property2"],
               nil);
  STAssertTrue([message setValue:@"property3Value"
                     forProperty:@"property3"],
               nil);

  NSString *string = [formatter stringForMessage:message];
  NSRange property1Range = [string rangeOfString:@"property1Value"];
  STAssertTrue(property1Range.location != NSNotFound, nil);
  NSRange property2Range = [string rangeOfString:@"property2Value"];
  STAssertTrue(property2Range.location != NSNotFound, nil);
  NSRange property3Range = [string rangeOfString:@"property3Value"];
  STAssertTrue(property3Range.location != NSNotFound, nil);
}

- (void)testPropertyFormatter
{
  KPTASLMessage *message = [KPTASLMessage message];
  STAssertTrue([message setValue:@"property1Value"
                     forProperty:@"property1"],
               nil);
  STAssertTrue([message setValue:@"property2Value"
                     forProperty:@"property2"],
               nil);
  STAssertTrue([message setValue:@"property3Value"
                     forProperty:@"property3"],
               nil);
  STAssertTrue([message setValue:@"property4Value"
                     forProperty:@"property4"],
               nil);

  NSArray *properties = [NSArray array];
  id<KPTASLReaderFormatter> formatter =
    [KPTASLReaderPropertyFormatter propertyFormatterWithProperties:properties];
  STAssertNotNil(formatter, nil);
  STAssertEqualObjects([formatter stringForMessage:message], @"", nil);

  // Test properties that are not present.
  properties = [NSArray arrayWithObjects:@"foo", @"bar", @"baz", nil];
  STAssertNotNil(properties, nil);
  formatter =
    [KPTASLReaderPropertyFormatter propertyFormatterWithProperties:properties];
  STAssertNotNil(formatter, nil);
  STAssertEqualObjects([formatter stringForMessage:message], @"", nil);

  properties = [NSArray arrayWithObjects:
                @"property2", @"property3", @"property1", nil];
  STAssertNotNil(properties, nil);
  formatter =
    [KPTASLReaderPropertyFormatter propertyFormatterWithProperties:properties];
  STAssertNotNil(formatter, nil);
  NSString *string = [formatter stringForMessage:message];

  // Test that given properties are collected.
  NSRange property1Range = [string rangeOfString:@"property1Value"];
  STAssertTrue(property1Range.location != NSNotFound, nil);
  NSRange property2Range = [string rangeOfString:@"property2Value"];
  STAssertTrue(property2Range.location != NSNotFound, nil);
  NSRange property3Range = [string rangeOfString:@"property3Value"];
  STAssertTrue(property3Range.location != NSNotFound, nil);
  NSRange property4Range = [string rangeOfString:@"property4Value"];
  STAssertTrue(property4Range.location == NSNotFound, nil);

  // Test that properties are collected in the correct order.
  STAssertTrue(property2Range.location < property3Range.location, nil);
  STAssertTrue(property3Range.location < property1Range.location, nil);
}

@end
