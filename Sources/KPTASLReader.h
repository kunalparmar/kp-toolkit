//
//  KPTASLReader.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <Foundation/Foundation.h>

@class KPTASLMessage, KPTASLQuery;
@protocol KPTASLReaderWriter, KPTASLReaderFormatter;

//
// KPTASLReader
//

@interface KPTASLReader : NSOperation
{
  KPTASLQuery *query_;
  id<KPTASLReaderWriter> writer_;
  id<KPTASLReaderFormatter> formatter_;
}

// Returns an autoreleased instance of the operation that will collect the
// logs from running a given |query|.
//
// Parameters
//  query
//    The query to use for collecting the logs. If |query| is nil, returns nil.
//  writer
//    The writer to write the collected logs to. If |writer| is nil,
//    [NSFileHandle fileHandleWithStandardOutput] will be used.
//  formatter
//    The formatter to use for writing the collected logs. If |formatter| is
//    nil, KPTASLReaderBasicFormatter is used.
//
// Return Value
//  A reader with a given |query|.
+ (id)readerWithQuery:(KPTASLQuery *)query
               writer:(id<KPTASLReaderWriter>)writer
            formatter:(id<KPTASLReaderFormatter>)formatter;

@end

#pragma mark -
#pragma mark KPTASLReaderWriter

//
// KPTASLReaderWriter
//

// Protocol to be implemented by a KPTASLReaderWriter instance.
@protocol KPTASLReaderWriter <NSObject>

// Writes the given |message| to where the writer is configured to write.
- (void)write:(NSString *)message;

@end


// Category on NSFileHandle that makes NSFileHandle valid writers.
@interface NSFileHandle (KPTASLReaderWriter) <KPTASLReaderWriter>

// Returns an autoreleased instance by opening the file at |path| in append
// mode, creating it with |mode| if it did not exist.
//
// Parameters
//  path
//    The path of the file to open. If |path| is nil, returns nil.
//  mode
//    The mode to create the file at path in if it did not exist.
//
// Return Value
//  A file handle for the file at path or nil if the file could not be created.
+ (id)fileHandleForWritingAtPath:(NSString *)path
                            mode:(mode_t)mode;

@end

#pragma mark -
#pragma mark KPTASLReaderFormatter

//
// KPTASLReaderFormatter
//

@protocol KPTASLReaderFormatter <NSObject>

// Returns a formatted string for a given |message|.
- (NSString *)stringForMessage:(KPTASLMessage *)message;

@end


// A basic formatter that creates a string for a |message| using all the
// properties of the |message|.
@interface KPTASLReaderBasicFormatter : NSObject <KPTASLReaderFormatter>
{
  NSNumberFormatter *numberFormatter_;
  NSDateFormatter *dateFormatter_;
}

// Returns an autoreleased instance.
+ (id)basicFormatter;

// Returns a formatted string for given |value| corresponding to given
// |property|.
//
// Parameters
//  property
//    The property whose value is to be formatted.
//  values
//    The value to be formatted.
//
// Return Value
//  A formatted string for given |value| correponding to give |property|.
- (NSString *)stringForMessageProperty:(NSString *)property
                                 value:(NSString *)value;

@end


// Formatter that creates a string for a |message| using a specified set of
// properties of the |message|.
@interface KPTASLReaderPropertyFormatter : KPTASLReaderBasicFormatter
{
  NSArray *properties_;
}

// Returns an autoreleased instance with a given array of properties.
//
// Parameters
//  properties
//    An array of NSString's which are the properties of the message that will
//    be used when formatting a message. The order of the properties will be
//    the same as the order in the array.
//
// Return Value
//  A property formatter with given properties.
+ (id)propertyFormatterWithProperties:(NSArray *)properties;

@end
