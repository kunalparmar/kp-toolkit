//
//  KPTASLQueryTest.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <SenTestingKit/SenTestingKit.h>

#import "KPTASLQuery.h"

@interface KPTASLQueryTest : SenTestCase
{
  KPTASLQuery *query;
}

@end

@implementation KPTASLQueryTest

- (void)setUp
{
  query = [[KPTASLQuery query] retain];
  STAssertNotNil(query, nil);
}

- (void)tearDown
{
  STAssertNotNil(query, nil);
  [query release];
  query = nil;
}

- (void)testValueForProperty
{
  STAssertThrowsSpecificNamed([query valueForProperty:nil],
                              NSException,
                              NSInvalidArgumentException,
                              nil);
  STAssertNil([query valueForProperty:@"foo"], nil);
}

- (void)testSetValueOperationForProperty
{
  STAssertThrowsSpecificNamed([query setValue:@"foo"
                                    operation:kKPTASLQueryOperationEqual
                                  forProperty:nil],
                              NSException,
                              NSInvalidArgumentException,
                              nil);

  STAssertTrue([query setValue:@"foo"
                     operation:kKPTASLQueryOperationLess
                   forProperty:@"bar"],
               nil);
  STAssertTrue([query setValue:@"blah"
                     operation:kKPTASLQueryOperationGreater
                   forProperty:@"baz"],
               nil);
  STAssertEqualObjects([query valueForProperty:@"bar"], @"foo", nil);
  STAssertEqualObjects([query valueForProperty:@"baz"], @"blah", nil);

  STAssertTrue([query setValue:nil
                     operation:kKPTASLQueryOperationNotEqual
                   forProperty:@"bar"],
               nil);
  STAssertNil([query valueForProperty:@"bar"], nil);
}

@end
