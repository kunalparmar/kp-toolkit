//
//  KPTNSObject+Foundation.m
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "KPTNSObject+Foundation.h"

#import <objc/objc-runtime.h>

static void MissingMethodError(Class klass, SEL selector, NSError **error)
{
  if (error == nil)
  {
    return;
  }

  NSString *description = [NSString stringWithFormat:@"Method %@ not implemented by class %@",
                           NSStringFromSelector(selector),
                           NSStringFromClass(klass)];
  NSDictionary *userInfo = [NSDictionary dictionaryWithObject:description
                                                       forKey:NSLocalizedDescriptionKey];
  *error = [NSError errorWithDomain:NSCocoaErrorDomain
                               code:-1
                           userInfo:userInfo];
}


@implementation NSObject (KPTNSObjectFoundation)

+ (BOOL)swizzleClassMethod:(SEL)originalSelector
                withMethod:(SEL)newSelector
                     error:(NSError **)error
{
  return [object_getClass(self) swizzleInstanceMethod:originalSelector
                                           withMethod:newSelector
                                                error:error];
}

+ (BOOL)swizzleInstanceMethod:(SEL)originalSelector
                   withMethod:(SEL)newSelector
                        error:(NSError **)error
{
  Method originalMethod = class_getInstanceMethod(self, originalSelector);
  if (originalMethod == nil)
  {
    MissingMethodError(self, originalSelector, error);
    return NO;
  }

  Method newMethod = class_getInstanceMethod(self, newSelector);
  if (newMethod == nil)
  {
    MissingMethodError(self, newSelector, error);
    return NO;
  }

  // There are two scenarios:
  //  a. the method to swizzle i.e. |originalMethod| is not implemented by the
  //     class, but is inherited from a superclass
  //  b. the method to swizzle is implemented by the class
  //
  // For a), |originalMethod| is added to the class with the implementation of
  // |newMethod| i.e. in the swizzled state and then the implementation of
  // |newMethod| is replaced by that of |originalMethod|.
  // NOTE: |class_addMethod| will add an override of a superclass's
  // implementation and return YES, but will not replace an existing
  // implementation in the class, returning NO.
  //
  // For b), the implementations of the methods are simply swapped.
  if (class_addMethod(self,
                      originalSelector,
                      method_getImplementation(newMethod),
                      method_getTypeEncoding(newMethod)))
  {
    class_replaceMethod(self,
                        newSelector,
                        method_getImplementation(originalMethod),
                        method_getTypeEncoding(originalMethod));
  }
  else
  {
    method_exchangeImplementations(originalMethod, newMethod);
  }

  return YES;
}

- (void)coalescedPerformSelector:(SEL)aSelector
                      withObject:(id)anArgument
                      afterDelay:(NSTimeInterval)delay
{
  [self retain];
  [anArgument retain];
  [[self class] cancelPreviousPerformRequestsWithTarget:self
                                               selector:aSelector
                                                 object:anArgument];
  [self performSelector:aSelector
             withObject:anArgument
             afterDelay:delay];
  [anArgument release];
  [self release];
}

@end
