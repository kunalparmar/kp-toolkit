//
//  KPTNSObject+FoundationTest.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <SenTestingKit/SenTestingKit.h>

#import "KPTNSObject+Foundation.h"

@interface TestClassForSwizzlingBase : NSObject

+ (NSString *)inheritedClassMethod;
+ (NSString *)overridenClassMethod;
- (NSString *)inheritedInstanceMethod;
- (NSString *)overridenInstanceMethod;

@end

@implementation TestClassForSwizzlingBase

+ (NSString *)inheritedClassMethod
{
  return @"inheritedClassMethod";
}

+ (NSString *)overridenClassMethod
{
  return nil;
}

- (NSString *)inheritedInstanceMethod
{
  return @"inheritedInstanceMethod";
}

- (NSString *)overridenInstanceMethod
{
  return nil;
}

@end


@interface TestClassForSwizzling : TestClassForSwizzlingBase

+ (NSString *)overridenClassMethod;
- (NSString *)overridenInstanceMethod;

@end

@implementation TestClassForSwizzling

+ (NSString *)overridenClassMethod
{
  return @"overridenClassMethod";
}

- (NSString *)overridenInstanceMethod
{
  return @"overridenInstanceMethod";
}

@end


@interface TestClassForSwizzling (SwizzledMethod)

+ (NSString *)swizzledInheritedClassMethod;
+ (NSString *)swizzledOverridenClassMethod;
- (NSString *)swizzledInheritedInstanceMethod;
- (NSString *)swizzledOverridenInstanceMethod;

@end

@implementation TestClassForSwizzling (SwizzledMethod)

+ (NSString *)swizzledInheritedClassMethod
{
  return @"swizzledInheritedClassMethod";
}

+ (NSString *)swizzledOverridenClassMethod
{
  return @"swizzledOverridenClassMethod";
}

- (NSString *)swizzledInheritedInstanceMethod
{
  return @"swizzledInheritedInstanceMethod";
}

- (NSString *)swizzledOverridenInstanceMethod
{
  return @"swizzledOverridenInstanceMethod";
}

@end


@interface KPTNSObject_FoundationTest : SenTestCase
@end

@implementation KPTNSObject_FoundationTest

- (void)testSwizzlingClassMethodOverridenByClass
{
  STAssertEqualObjects([TestClassForSwizzling overridenClassMethod],
                       @"overridenClassMethod",
                       @"Overriden class method not setup correctly");
  NSError *error = nil;
  STAssertTrue([TestClassForSwizzling swizzleClassMethod:@selector(overridenClassMethod)
                                              withMethod:@selector(swizzledOverridenClassMethod)
                                                   error:&error],
               @"Failed to swizzle overriden class method; error %@", error);
  STAssertEqualObjects([TestClassForSwizzling overridenClassMethod],
                       @"swizzledOverridenClassMethod",
                       @"Overriden class method returned wrong value after swizzling");
  STAssertEqualObjects([TestClassForSwizzling swizzledOverridenClassMethod],
                       @"overridenClassMethod",
                       @"Overriden class method returned wrong value after swizzling");

  STAssertNil([TestClassForSwizzlingBase overridenClassMethod],
              @"Overriden class method was swizzled in base class");
}

- (void)testSwizzlingClassMethodInheritedByClass
{
  STAssertEqualObjects([TestClassForSwizzling inheritedClassMethod],
                       @"inheritedClassMethod",
                       @"Inherited class method not setup correctly");
  NSError *error = nil;
  STAssertTrue([TestClassForSwizzling swizzleClassMethod:@selector(inheritedClassMethod)
                                              withMethod:@selector(swizzledInheritedClassMethod)
                                                   error:&error],
               @"Failed to swizzle inherited class method; error %@", error);
  STAssertEqualObjects([TestClassForSwizzling inheritedClassMethod],
                       @"swizzledInheritedClassMethod",
                       @"Inherited class method returned wrong value after swizzling");
  STAssertEqualObjects([TestClassForSwizzling swizzledInheritedClassMethod],
                       @"inheritedClassMethod",
                       @"Inherited class method returned wrong value after swizzling");

  STAssertEqualObjects([TestClassForSwizzlingBase inheritedClassMethod],
                       @"inheritedClassMethod",
                       @"Inherited class method was swizzled in base class");
}

- (void)testSwizzlingUnimplementedClassMethodFails
{
  NSError *error = nil;
  STAssertFalse([NSObject swizzleClassMethod:@selector(inheritedClassMethod)
                                  withMethod:@selector(version)
                                       error:nil],
                @"Swizzling unimplemented class method succeeded");
  STAssertFalse([NSObject swizzleClassMethod:@selector(inheritedClassMethod)
                                  withMethod:@selector(version)
                                       error:&error],
                @"Swizzling unimplemented class method succeeded");
  STAssertNotNil(error,
                 @"Swizzling unimplemented class method did not return error");

  error = nil;
  STAssertFalse([NSObject swizzleClassMethod:@selector(version)
                                  withMethod:@selector(inheritedClassMethod)
                                       error:nil],
                @"Swizzling with unimplemented class method succeeded");
  STAssertFalse([NSObject swizzleClassMethod:@selector(version)
                                  withMethod:@selector(inheritedClassMethod)
                                       error:&error],
                @"Swizzling with unimplemented class method succeeded");
  STAssertNotNil(error,
                 @"Swizzling with unimplemented class method did not return error");
}

- (void)testSwizzlingInstanceMethodOverridenByClass
{
  TestClassForSwizzling *testClass = [[[TestClassForSwizzling alloc] init] autorelease];
  STAssertEqualObjects([testClass overridenInstanceMethod],
                       @"overridenInstanceMethod",
                       @"Overriden instance method not setup correctly");
  NSError *error = nil;
  STAssertTrue([TestClassForSwizzling swizzleInstanceMethod:@selector(overridenInstanceMethod)
                                                 withMethod:@selector(swizzledOverridenInstanceMethod)
                                                      error:&error],
               @"Failed to swizzle overriden instance method; error %@", error);
  STAssertEqualObjects([testClass overridenInstanceMethod],
                       @"swizzledOverridenInstanceMethod",
                       @"Overriden instance method returned wrong value after swizzling");
  STAssertEqualObjects([testClass swizzledOverridenInstanceMethod],
                       @"overridenInstanceMethod",
                       @"Overriden instance method returned wrong value after swizzling");

  TestClassForSwizzlingBase *testBaseClass = [[[TestClassForSwizzlingBase alloc] init] autorelease];
  STAssertNil([testBaseClass overridenInstanceMethod],
              @"Overriden instance method was swizzled in base class");
}

- (void)testSwizzlingInstanceMethodInheritedByClass
{
  TestClassForSwizzling *testClass = [[[TestClassForSwizzling alloc] init] autorelease];
  STAssertEqualObjects([testClass inheritedInstanceMethod],
                       @"inheritedInstanceMethod",
                       @"Inherited instance method not setup correctly");
  NSError *error = nil;
  STAssertTrue([TestClassForSwizzling swizzleInstanceMethod:@selector(inheritedInstanceMethod)
                                                 withMethod:@selector(swizzledInheritedInstanceMethod)
                                                      error:&error],
               @"Failed to swizzle overriden instance method; error %@", error);
  STAssertEqualObjects([testClass inheritedInstanceMethod],
                       @"swizzledInheritedInstanceMethod",
                       @"Overriden instance method returned wrong value after swizzling");
  STAssertEqualObjects([testClass swizzledInheritedInstanceMethod],
                       @"inheritedInstanceMethod",
                       @"Overriden instance method returned wrong value after swizzling");

  TestClassForSwizzlingBase *testBaseClass = [[[TestClassForSwizzlingBase alloc] init] autorelease];
  STAssertEqualObjects([testBaseClass inheritedInstanceMethod],
                       @"inheritedInstanceMethod",
                       @"Inherited instance method was swizzled in base class");
}

- (void)testSwizzlingUnimplementedInstanceMethodFails
{
  NSError *error = nil;
  STAssertFalse([NSObject swizzleInstanceMethod:@selector(inheritedInstanceMethod)
                                     withMethod:@selector(description)
                                          error:nil],
                @"Swizzling unimplemented instance method succeeded");
  STAssertFalse([NSObject swizzleInstanceMethod:@selector(inheritedInstanceMethod)
                                     withMethod:@selector(description)
                                          error:&error],
                @"Swizzling unimplemented instance method succeeded");
  STAssertNotNil(error,
                 @"Swizzling unimplemented instance method did not return error");

  error = nil;
  STAssertFalse([NSObject swizzleClassMethod:@selector(description)
                                  withMethod:@selector(inheritedInstanceMethod)
                                       error:nil],
                @"Swizzling with unimplemented instance method succeeded");
  STAssertFalse([NSObject swizzleClassMethod:@selector(description)
                                  withMethod:@selector(inheritedInstanceMethod)
                                       error:&error],
                @"Swizzling with unimplemented instance method succeeded");
  STAssertNotNil(error,
                 @"Swizzling with unimplemented instance method did not return error");
}

- (void)testKPT_STRING_FOR_KEY
{
  STAssertEquals(KPT_STRING_FOR_KEY(retainCount, NSObject),
                 @"retainCount",
                 @"retainCount key not returned");

  // Cannot test failure since this is compile time check.
}

- (void)testKPT_DYNAMIC_CAST
{
  NSMutableArray *array = [NSMutableArray array];
  STAssertTrue(KPT_DYNAMIC_CAST(NSArray, array) == array,
               @"Dynamic cast NSMutableArray -> Array failed");
  STAssertNil(KPT_DYNAMIC_CAST(NSString, array),
              @"Dynamic cast NSMutableArray -> NSString succeeded");
}

@end
