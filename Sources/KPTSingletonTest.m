//
//  KPTSingletonTest.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <SenTestingKit/SenTestingKit.h>

#import "KPTSingleton.h"

@interface TestDirManager : NSObject

KPT_DECLARE_SINGLETON_FOR_CLASS_WITH_ACCESSOR(TestDirManager, defaultManager);

@end

@implementation TestDirManager

KPT_SYNTHESIZE_SINGLETON_FOR_CLASS_WITH_ACCESSOR(TestDirManager, defaultManager);

@end


@interface TestFileManager : NSObject

KPT_DECLARE_SINGLETON_FOR_CLASS(TestFileManager);

@end

@implementation TestFileManager

KPT_SYNTHESIZE_SINGLETON_FOR_CLASS(TestFileManager);

@end


@interface KPTSingletonTest : SenTestCase

@end

@implementation KPTSingletonTest

- (void)testSingletonCreatedOnFirstAccess
{
  STAssertNotNil([TestDirManager defaultManager], nil);
  STAssertNotNil([TestFileManager sharedTestFileManager], nil);
}

- (void)testSingletonCreatedUsingInit
{
  STAssertNotNil([[[TestDirManager alloc] init] autorelease], nil);
  STAssertNotNil([[[TestFileManager alloc] init] autorelease], nil);
}

- (void)testSameObjectReturnedOnMultipleAccess
{
  TestDirManager *dirManager = [TestDirManager defaultManager];
  STAssertNotNil(dirManager, nil);
  STAssertTrue([TestDirManager defaultManager] == dirManager, nil);

  TestFileManager *fileManager = [TestFileManager sharedTestFileManager];
  STAssertNotNil(fileManager, nil);
  STAssertTrue([TestFileManager sharedTestFileManager] == fileManager, nil);
}

@end
