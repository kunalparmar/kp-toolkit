//
//  KPTNSObject+Foundation.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <Foundation/Foundation.h>

// A macro for compile time checks to make sure the property being accessed
// using key-value coding is implemented by a class.
//
// Discussion
//  -[NSObject valueForKey:] raises an exception when it does not find a
//  property corresponding to a given key. This happens at runtime and will most
//  likely cause the program to crash. This macros avoids these runtime
//  crashes by checking at compile time whether a given class has a property for
//  a given key.
//
//  Example, -[MyArray valueForKey:@"name"] would be replaced by
//  -[MyArray valueForKey:KPT_STRING_FOR_KEY(name, MyClass)].
//
//  To ensure that performance is not affected, this macro is a no-op in
//  non-Debug builds.
#ifdef DEBUG
  #define KPT_STRING_FOR_KEY(key, class) ([(class *)nil key], @#key)
#else
  #define KPT_STRING_FOR_KEY(key, class) @#key
#endif // DEBUG


// A macro for safe dynamic cast at runtime in Objective-C.
//
// Discussion
//  The dynamic cast will return nil if the object is not of the expected class.
//  This is safe in Objective-C since sending messages to nil is ok. On the
//  other hand, if a message was sent to an unexpected class and that message
//  was not implemented by the class, it will raise an exception and most
//  likely cause the program to crash.
#define KPT_DYNAMIC_CAST(type, object) \
  ([object isKindOfClass:[type class]] ? object : nil)


@interface NSObject (KPTNSObjectFoundation)

// Swizzle a class method.
//
// Discussion
//  Objective-C categories allow you to replace the behavior of any class. But
//  often times, you dont want to replace, but instead want to add behavior.
//  Using categories, you do not have access to the original implementation.
//  Swizzling allows you to do that.
//
//  Example, you want to detect if there are memory leaks. You can swizzle the
//  +alloc and -dealloc methods and keep track of a counter to determine if
//  any instances were left behind.
//
//    @implementation MyClass (Swizzling)
//
//    + (id)swizzledAlloc {
//      counter++;
//
//      // Call the original code.
//      return [self swizzledAlloc];
//    }
//
//    - (void)swizzledDealloc {
//      counter--;
//
//      // Call the original code.
//      [self swizzledDealloc];
//    }
//
//    @end
//
//    int counter = 0;
//    main() {
//      [MyClass swizzleClassMethod:@selector(alloc)
//                       withMethod:@selector(swizzledAlloc)
//                            error:nil];
//      [MyClass swizzleInstanceMethod:@selector(dealloc)
//                          withMethod:@selector(swizzledDealloc)
//                               error:nil];
//
//       ...
//
//      assert(counter == 0);
//    }
//
//  NOTE: to call the original code, you call the swizzledAlloc method. This
//  works because the implementation of swizzledAlloc method points to the
//  original code after swizzling.
//
//  IMPORTANT:
//  Swizzling does not work in a world where you have multiple classes deriving
//  from one base class and you are trying to swizzle methods in them. You will
//  run into strange bugs if you do that.
+ (BOOL)swizzleClassMethod:(SEL)originalSelector
                withMethod:(SEL)newSelector
                     error:(NSError **)error;

// Swizzle an instance method.
//
// Discussion
//  See |+swizzleClassMethod:withMethod:error:| for details.
+ (BOOL)swizzleInstanceMethod:(SEL)originalSelector
                   withMethod:(SEL)newSelector
                        error:(NSError **)error;

// Safer way of doing +cancelPreviousPerformRequestsWithTarget:selector:object:
// followed by -performSelector:withObject:afterDelay:.
//
// Dicussion
//  -performSelector:withObject:afterDelay: retains self and the argument passed
//  to it. If you call
//  +cancelPreviousPerformRequestsWithTarget:selector:object:, it will release
//  self and the argument which may cause them to be deallocated. Using those
//  objects further might cause a crash.
//
//  This method retains self and the argument before calling
//  +cancelPreviousPerformRequestsWithTarget:selector:object:, which ensures
//  that the objects will not get deallocated, then calls
//  -performSelector:withObject:afterDelay: and then releases self and the
//  argument.
- (void)coalescedPerformSelector:(SEL)aSelector
                      withObject:(id)anArgument
                      afterDelay:(NSTimeInterval)delay;

@end
