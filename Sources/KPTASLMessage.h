//
//  KPTASLMessage.h
//
//  Copyright 2012 Kunal Parmar
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <Foundation/Foundation.h>

#import "KPTASL.h"

// Wrapper for aslmsg structure. Use -valueForKey: and -setValue:forKey: to
// access the message attributes.
@interface KPTASLMessage : NSObject
{
  aslmsg message_;
  BOOL nofree_;
}

@property (nonatomic, readonly) aslmsg message;

// Returns an autoreleased instance.
+ (id)message;

// Returns an autoreleased instance created using the aslmsg structure returned
// by a response.
//
// Parameters
//  message
//    The aslmessage structure to use. If |message| is NULL, returns nil.
//
// Discussion
//  The aslmsg structure is not released when the message is released. The
//  response that created it takes care of managing the memory.
//
// Return Value
//  A message created using a given aslmsg structure.
+ (id)messageWithResponseMessage:(aslmsg)message;

// Returns the value of a given |property|.
//
// Parameters
//  property
//    The property whose value will be returned. Cannot be nil. Raises an
//    exception is |property| is nil.
- (NSString *)valueForProperty:(NSString *)property;

// Sets the value of a given |property|.
//
// Parameters
//  value
//    The value to set for given property.
//  property
//    The property whose value will be set. Cannot be nil. Raises an exception
//    if |property| is nil.
//
// Return Value
//  YES if |value| was set successfully; otherwise, NO.
- (BOOL)setValue:(NSString *)value
     forProperty:(NSString *)property;

// Returns an NSString-keyed dictionary of the key/value pairs in the message.
- (NSDictionary *)properties;

@end
